package com.presenthelper.isep.presenthelper.api;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.presenthelper.isep.presenthelper.api.entities.Presentation;
import com.presenthelper.isep.presenthelper.firebase.Registration;
import com.presenthelper.isep.presenthelper.tools.InputStreamVolleyRequest;
import com.presenthelper.isep.presenthelper.tools.MultipartRequest;
import com.presenthelper.isep.presenthelper.tools.MyProperties;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

public class MainApi {
    private static MainApi ourInstance;

    //On emulator
    //private final String _apiUri = "http://10.0.2.2:8080";
    //On divice
    private final String _apiUri = "http://52.226.77.190:8084";
    private final String client = "testjwtclientid";
    private final String secret = "XY7kmzoNzl100";
    private RequestQueue _queue;
    private String _apiToken;
    private String _apiTokenType;
    private Calendar _apiTokenExpirationDate;
    private Map<String, Presentation> _presentationsMap;
    private String _username;
    private String _password;
    private Context _context;

    public static MainApi getInstance(Context context) {
        return (ourInstance != null) ? ourInstance : new MainApi(context);
    }

    private MainApi(Context context) {
        _context = context;
        _presentationsMap = new HashMap<>();
        _queue = Volley.newRequestQueue(context);
        _queue.start();
    }

    public void requestToken(final String username, final String password,
                             final Runnable successCallback, final Runnable failureCallback) {
        getAuthToken(username, password, successCallback, failureCallback);

    }

    public void registerUser(final String firstName, final String lastName, final String username,
                             final String password, final Runnable successCallback, final Runnable failureCallback) {
        final String url = _apiUri + "/users";

        Map<String, String>  params = new HashMap<>();
        params.put("firstName", firstName);
        params.put("lastName", lastName);
        params.put("username", username);
        params.put("password", password);
        JSONObject jsonBody = new JSONObject(params);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        getAuthToken(username, password, successCallback, failureCallback);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                        failureCallback.run();
                    }
                }
        );
        _queue.add(request);
    }

    public void addUserAsPresenter(String username, String password, final Runnable successCallback, final Runnable failureCallback){
        final String url = _apiUri + "/user/presenter";
        final String token = MyProperties.getInstance().token;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        successCallback.run();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                        failureCallback.run();

                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+token);
                headers.put("Accept","application/json");
                return headers;
            }
        };
        _queue.add(request);

    }
    public void addUserAsViewr(String username, String password,final Runnable successCallback, final Runnable failureCallback){
        final String url = _apiUri + "/user/viewer";
        final String token = MyProperties.getInstance().token;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        successCallback.run();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                        failureCallback.run();
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+token);
                headers.put("Accept","application/json");
                return headers;
            }
        };
        _queue.add(request);
    }

    public void getAuthToken(final String username, final String password, final Runnable successCallback, final Runnable failureCallback){


        final String url = _apiUri + "/oauth/token";

        _username = username;
        _password = password;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    _apiToken = response.getString("access_token");
                    _apiTokenType = response.getString("token_type");
                    MyProperties.getInstance().token = _apiToken;
                    _apiTokenExpirationDate = Calendar.getInstance();
                    _apiTokenExpirationDate.add(Calendar.SECOND, response.getInt("expires_in"));
                    Registration regis = new Registration();
                    regis.context = _context;
                    regis.authToken = _apiToken;
                    regis.onTokenRefresh();
                    successCallback.run();
                } catch (JSONException e) {
                    e.printStackTrace();
                    failureCallback.run();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                failureCallback.run();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", getBasicAuthorization(client, secret));
                return headers;
            }

            @Override
            public byte[] getBody() {
                // init parameters
                Map<String, String> params = new HashMap<>();
                params.put("grant_type", "password");
                params.put("username", username);
                params.put("password", password);
                // encode parameters (can use Uri.Builder as above)
                String paramsEncoding = "UTF-8";
                StringBuilder encodedParams = new StringBuilder();
                try {
                    for (Map.Entry<String, String> entry : params.entrySet()) {
                        encodedParams.append(URLEncoder.encode(entry.getKey(), paramsEncoding));
                        encodedParams.append('=');
                        encodedParams.append(URLEncoder.encode(entry.getValue(), paramsEncoding));
                        encodedParams.append('&');
                    }
                    return encodedParams.toString().getBytes(paramsEncoding);
                } catch (UnsupportedEncodingException uee) {
                    throw new RuntimeException("Encoding not supported: " + paramsEncoding, uee);
                }
            }
        };

        _queue.add(jsonObjectRequest);
    }

    public void getPresentations( final Runnable successCallback, final Runnable failureCallback) {
        final String url = _apiUri + "/user/presentations";
        final String token = MyProperties.getInstance().token;

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //TODO Show presentations on screen
                        try {
                            _presentationsMap.clear();
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject presentation = response.getJSONObject(i);
                                _presentationsMap.put(presentation.getString("id"),new Presentation(presentation.getString("name"), presentation.getString("id")));
                            }

                            successCallback.run();
                        }catch (JSONException e){
                            e.printStackTrace();
                            failureCallback.run();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                        failureCallback.run();
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+token);
                headers.put("Accept","application/json");
                return headers;
            }
        };
        _queue.add(request);
    }

    public void createPresentation(final String name, final String filename, final Runnable successCallback, final Runnable failureCallback) {
        final String url = _apiUri + "/presentation";
        final String token = MyProperties.getInstance().token;

        Map<String, String>  params = new HashMap<>();
        params.put("name", name);
        JSONObject jsonBody = new JSONObject(params);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, url, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String presentationId = response.getString("id");
                            uploadfileToPresentation(presentationId, filename, successCallback, failureCallback);
                            successCallback.run();
                        }catch (JSONException je){
                            failureCallback.run();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                        failureCallback.run();
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+token);
                headers.put("Accept","application/json");
                return headers;
            }
        };
        _queue.add(request);
    }

    public void uploadfileToPresentation(String presentationId, String filename, final Runnable successCallback, final Runnable failureCallback){
        //TODO missing file upload
        final String url = _apiUri + "/presentation/"+presentationId;
        final String token = MyProperties.getInstance().token;


        File sdcard = Environment.getExternalStorageDirectory();

        //On emulator
        //filename = filename.replace("/document/raw:/storage/emulated/0/","");
        //On device
        filename = filename.replace("document/primary:","");
        File file = new File(sdcard,filename);

        MultipartRequest multipartRequest = new MultipartRequest(url, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                failureCallback.run();
            }
        }, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                successCallback.run();
            }
        }, file, "file"){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+token);
                headers.put("Accept","application/json");
                return headers;
            }
        };

        _queue.add(multipartRequest);


    }
    public void editPresentation(final String presentation, final Runnable successCallback, final Runnable failureCallback) {
        final String url = _apiUri + "/";

        Map<String, String>  params = new HashMap<>();
        //params.put("", name);
        JSONObject jsonBody = new JSONObject(params);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        successCallback.run();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                        failureCallback.run();
                    }
                }
        );
        _queue.add(request);
    }

    public void deletePresentation(final String presentation, final Runnable successCallback, final Runnable failureCallback) {
        final String url = _apiUri + "/";

        Map<String, String>  params = new HashMap<>();
        //params.put("", name);
        JSONObject jsonBody = new JSONObject(params);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE, url, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        successCallback.run();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                        failureCallback.run();
                    }
                }
        );
        _queue.add(request);
    }

    public void subscribeLivePresentation(final String presentation, final Runnable successCallback, final Runnable failureCallback) {
        final String url = _apiUri + "/presentation/"+presentation+"/subscribe";
        final String token = MyProperties.getInstance().token;



        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        successCallback.run();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                        failureCallback.run();
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+token);
                headers.put("Accept","application/json");
                return headers;
            }
        };
        _queue.add(request);
    }

    public void unsubscribeLivePresentation(final String presentation, final Runnable successCallback, final Runnable failureCallback) {
        final String url = _apiUri + "/";

        Map<String, String>  params = new HashMap<>();
        //params.put("", name);
        JSONObject jsonBody = new JSONObject(params);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        successCallback.run();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                        failureCallback.run();
                    }
                }
        );
        _queue.add(request);
    }
    public void updateToken(final String fbToken,final String authToken, final Runnable successCallback, final Runnable failureCallback) {
        final String url = _apiUri + "/user/token";
        final String token = MyProperties.getInstance().token;

        Map<String, String>  params = new HashMap<>();
        params.put("token", fbToken);
        JSONObject jsonBody = new JSONObject(params);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        successCallback.run();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                        failureCallback.run();
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+authToken);
                headers.put("Accept","application/json");
                return headers;
            }
        };
        _queue.add(request);
    }

    public void getPresentationNotes(final String presentation, final Runnable successCallback, final Runnable failureCallback) {
        final String url = _apiUri + "/";

        Map<String, String>  params = new HashMap<>();
        //params.put("", name);
        JSONObject jsonBody = new JSONObject(params);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        successCallback.run();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                        failureCallback.run();
                    }
                }
        );
        _queue.add(request);
    }

    public void getPresentationSlides(final String presentation, final Runnable successCallback, final Runnable failureCallback) {
        final String url = _apiUri + "/";

        Map<String, String>  params = new HashMap<>();
        //params.put("", name);
        JSONObject jsonBody = new JSONObject(params);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        successCallback.run();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                        failureCallback.run();
                    }
                }
        );
        _queue.add(request);
    }

    public void getCurrentSlide(final String presentation, final Runnable successCallback, final Runnable failureCallback) {
        final String url = _apiUri + "/presentation/"+presentation+"/live";

        final String token = MyProperties.getInstance().token;
         InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.GET, url, new Response.Listener<byte[]>() {
            @Override
            public void onResponse(byte[] response) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                try {
                    if (response!=null) {



                        String filename = "image.jpg";


                        try{
                            long lenghtOfFile = response.length;

                            //covert reponse to input stream
                            InputStream input = new ByteArrayInputStream(response);

                            //Create a file on desired path and write stream data to it
                            File path = Environment.getExternalStorageDirectory();
                            File file = new File(path, filename);
                            map.put("resume_path", file.toString());
                            BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file));
                            byte data[] = new byte[1024];

                            long total = 0;
                            int count;
                            while ((count = input.read(data)) != -1) {
                                total += count;
                                output.write(data, 0, count);
                            }

                            output.flush();

                            output.close();
                            input.close();
                        }catch(IOException e){
                            e.printStackTrace();
                            failureCallback.run();

                        }
                        successCallback.run();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Log.d("KEY_ERROR", "UNABLE TO DOWNLOAD FILE");
                    e.printStackTrace();
                    failureCallback.run();

                }

                successCallback.run();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                failureCallback.run();
            }
        }, null){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+token);
                headers.put("Accept","application/json");
                return headers;
            }
        };
        _queue.add(request);

    }

    public String getToken() {
        return _apiToken;
    }

    public boolean isTokenValid() {
        //return true;
 return _apiTokenExpirationDate != null
                && getToken() != null
                && _apiTokenExpirationDate.after(Calendar.getInstance());
    }

    public Map<String, Presentation> getPresentations() {
        return _presentationsMap;
    }

    public Presentation getPresentation(String key) {
        return _presentationsMap.get(key);
    }

    private String getBasicAuthorization(String username, String password) {
        String credentials = username + ":" + password;
        return "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
    }

    public void startLivePresentation(final String presentation, final Runnable successCallback, final Runnable failureCallback){
        final String url = _apiUri + "/presentation/"+presentation+"/live";
        final String token = MyProperties.getInstance().token;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        successCallback.run();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                        failureCallback.run();
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+token);
                headers.put("Accept","application/json");
                return headers;
            }
        };
        _queue.add(request);
    }

    public void nextSlide(final String presentation, final Runnable successCallback, final Runnable failureCallback){
        final String url = _apiUri + "/presentation/"+presentation+"/live/next";
        final String token = MyProperties.getInstance().token;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        successCallback.run();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                        failureCallback.run();
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+token);
                headers.put("Accept","application/json");
                return headers;
            }
        };
        _queue.add(request);
    }

    public void previusSlide(final String presentation, final Runnable successCallback, final Runnable failureCallback){
        final String url = _apiUri + "/presentation/"+presentation+"/live/previus";
        final String token = MyProperties.getInstance().token;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        successCallback.run();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                        failureCallback.run();
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+token);
                headers.put("Accept","application/json");
                return headers;
            }
        };
        _queue.add(request);
    }


    public void endPresentation(final String presentation, final Runnable successCallback, final Runnable failureCallback){
        final String url = _apiUri + "/presentation/"+presentation+"/live";
        final String token = MyProperties.getInstance().token;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        successCallback.run();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        e.printStackTrace();
                        failureCallback.run();
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+token);
                headers.put("Accept","application/json");
                return headers;
            }
        };
        _queue.add(request);
    }
}
