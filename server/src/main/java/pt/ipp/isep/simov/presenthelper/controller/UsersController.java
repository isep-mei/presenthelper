package pt.ipp.isep.simov.presenthelper.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pt.ipp.isep.simov.presenthelper.domain.Role;
import pt.ipp.isep.simov.presenthelper.domain.User;
import pt.ipp.isep.simov.presenthelper.repository.RoleRepository;
import pt.ipp.isep.simov.presenthelper.repository.UserRepository;

@RestController
@RequestMapping("/users")
public class UsersController {
	
	@Value("${security.encoding-strength}")
	private Integer encodingStrength;
	
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@RequestMapping(method = RequestMethod.GET)
	@PreAuthorize("isAuthenticated()")
	public User getUsers(Principal principal) {
		return userRepository.findByUsername(principal.getName());
	}

	@RequestMapping( method = RequestMethod.POST)
	public User createUser(@RequestBody User user) {
		ShaPasswordEncoder spe = new ShaPasswordEncoder(encodingStrength);
		user.setPassword(spe.encodePassword(user.getPassword(), null));
		List<Role> listRoles = new ArrayList<>();
		// TODO
		// Shuld implement the service insted of the repository
		listRoles.add(roleRepository.findByRoleName("STANDARD_USER"));
		user.setRoles(listRoles);
		return userRepository.save(user);
	}

}
