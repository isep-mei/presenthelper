package com.presenthelper.isep.presenthelper.firebase;

import android.content.Context;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.presenthelper.isep.presenthelper.api.MainApi;

import static android.content.ContentValues.TAG;

/**
 * Created by jpfelgueiras on 10/01/2018.
 */

public class Registration extends FirebaseInstanceIdService {

    private MainApi _mainApi;

    public Context context;

    public String authToken;



    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(final String refreshedToken) {
        _mainApi = MainApi.getInstance(context);
        _mainApi.updateToken(refreshedToken, authToken, new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Token updated: " + refreshedToken);
            }
        }, new Runnable() {
            @Override
            public void run() {
                //sendRegistrationToServer(refreshedToken);
            }
        });
    }
}
