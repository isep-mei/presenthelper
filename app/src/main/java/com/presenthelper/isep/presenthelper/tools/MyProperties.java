package com.presenthelper.isep.presenthelper.tools;

/**
 * Created by jpfelgueiras on 08/01/2018.
 */

public class MyProperties {

    private static MyProperties mInstance= null;

    public String token;

    protected MyProperties(){}

    public static synchronized MyProperties getInstance(){
        if(null == mInstance){
            mInstance = new MyProperties();
        }
        return mInstance;
    }
}
