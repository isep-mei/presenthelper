package com.presenthelper.isep.presenthelper.login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.StringRequest;
import com.presenthelper.isep.presenthelper.R;
import com.presenthelper.isep.presenthelper.api.MainApi;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int SIGNUP_ACTIVITY = 0;
    public static final String USERNAME_KEY = "username";
    public static final String PASSWORD_KEY = "password";

    private MainApi _mainApi;
    private EditText _usernameText;
    private EditText _passwordText;
    private Button _loginButton;
    private TextView _signupLink;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        _mainApi = MainApi.getInstance(this.getApplicationContext());
        _usernameText = (EditText) findViewById(R.id.input_username);
        _passwordText = (EditText) findViewById(R.id.input_password);
        _loginButton = (Button) findViewById(R.id.btn_login);
        _signupLink = (TextView) findViewById(R.id.link_signup);

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });

        _signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, SIGNUP_ACTIVITY);
            }
        });
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void login() {
        Log.d(TAG, "Login");

        String username = _usernameText.getText().toString();
        String password = _passwordText.getText().toString();

        if (!validate(username, password)) {
            onLoginFailed();
            return;
        }

        _loginButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        _mainApi.requestToken(username, password,
            new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                    if (_mainApi.isTokenValid()) {
                        onLoginSuccess();
                    } else {
                        onLoginFailed();
                    }
                }
            },
            new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                    onLoginFailed();
                }
            }
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case SIGNUP_ACTIVITY:
                signupHandler(resultCode, data);
                break;
        }
    }

    private void signupHandler(int resultCode, Intent data) {
        switch (resultCode) {
            case Activity.RESULT_OK:
                String username = data.getStringExtra(USERNAME_KEY);
                String password = data.getStringExtra(PASSWORD_KEY);
                if (username != null) { _usernameText.setText(username); }
                if (password != null) { _passwordText.setText(password); }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }

    private void onLoginSuccess() {
        _loginButton.setEnabled(true);
        setResult(RESULT_OK, null);
        finish();
    }

    private void onLoginFailed() {
        _loginButton.setEnabled(true);
       Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
    }

    private boolean validate(String username, String password) {
        boolean valid = true;

        if (username.isEmpty() || username.length() < 4) {
            _usernameText.setError("Enter a valid username.");
            valid = false;
        } else {
            _usernameText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4) {
            _passwordText.setError("Should be between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }
}