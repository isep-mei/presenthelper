package pt.ipp.isep.simov.presenthelper.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import pt.ipp.isep.simov.presenthelper.domain.Presentation;
import pt.ipp.isep.simov.presenthelper.domain.User;

public interface PresentationRepository extends CrudRepository<Presentation, Long>{
	List<Presentation> findByOwner(User user);
}
