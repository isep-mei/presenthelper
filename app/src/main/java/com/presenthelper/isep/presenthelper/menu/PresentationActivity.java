package com.presenthelper.isep.presenthelper.menu;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.presenthelper.isep.presenthelper.R;
import com.presenthelper.isep.presenthelper.api.MainApi;
import com.presenthelper.isep.presenthelper.api.entities.Presentation;

import java.io.File;

public class PresentationActivity extends AppCompatActivity {
    private static final String TAG = "PresentationActivity";
    private static final int FILE_CHOOSE_ACTIVITY = 0;

    public static final String MODE_KEY = "mode_type";
    public static final String PRESENTATION_KEY = "presentation_key";
    public static final int MODE_ADD = 0;
    public static final int MODE_EDIT = 1;

    private MainApi _mainApi;
    private EditText _nameInput;
    private Button _fileBtn;
    private TextView _filePathTxt;
    private Button _saveBtn;
    private int _mode;
    private Presentation _presentation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _mainApi = MainApi.getInstance(this.getApplicationContext());

        Bundle bundle = getIntent().getExtras();
        _mode = bundle.getInt(MODE_KEY);
        _presentation = _mainApi.getPresentation(bundle.getString(PRESENTATION_KEY));

        setContentView(R.layout.presentation);

        _nameInput = (EditText) findViewById(R.id.input_name);
        _fileBtn = (Button) findViewById(R.id.btn_choose_file);
        _filePathTxt = (TextView) findViewById(R.id.txt_file_path);
        _saveBtn = (Button) findViewById(R.id.btn_save);

        if(_mode == MODE_EDIT) {updateFields();}

        _fileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseFile;
                Intent intent;
                chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                chooseFile.setType("application/pdf");
                intent = Intent.createChooser(chooseFile, "Choose a file");
                startActivityForResult(intent, FILE_CHOOSE_ACTIVITY);
            }
        });

        _saveBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                createPresentation();
            }
        });
    }

    public void createPresentation(){
        String name = _nameInput.getText().toString();
        String filePath = _filePathTxt.getText().toString();

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

        if (!validate(name, filePath)) {
                onFailedSave();
                return;
            }
            _mainApi.createPresentation(name, filePath, new Runnable() {
                @Override
                public void run() {
                    onSuccessSave();
                }
            }, new Runnable() {
                @Override
                public void run() {
                    onFailedSave();
                }
            });
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    private void onSuccessSave() {
        setResult(RESULT_OK, null);
        finish();
    }


    private void onFailedSave() {
        Toast.makeText(getBaseContext(), "Unable to save Presentation", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_CHOOSE_ACTIVITY:
                if (resultCode != RESULT_OK) return;
                Uri uri = data.getData();
                _filePathTxt.setText(uri.getPath());
                break;
        }
    }

    private void updateFields() {
        _nameInput.setText(_presentation.getTitle());
    }

    private boolean validate(String name, String filePath) {
        boolean valid = true;

        if (name.isEmpty() || name.length() < 4) {
            _nameInput.setError("Enter a valid name.");
            valid = false;
        } else {
            _nameInput.setError(null);
        }

        if (filePath.isEmpty() && (new File(filePath).exists())) {
            _filePathTxt.setError("It should be a valid path");
            valid = false;
        } else {
            _filePathTxt.setError(null);
        }

        return valid;
    }
}
