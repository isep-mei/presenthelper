package pt.ipp.isep.simov.presenthelper.service;

import pt.ipp.isep.simov.presenthelper.domain.RandomCity;
import pt.ipp.isep.simov.presenthelper.domain.Role;
import pt.ipp.isep.simov.presenthelper.domain.User;

import java.util.List;

/**
 * Created by nydiarra on 06/05/17.
 */
public interface GenericService {
    User findByUsername(String username);

    List<User> findAllUsers();

    List<RandomCity> findAllRandomCities();
    
}
