package com.presenthelper.isep.presenthelper.menu;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.presenthelper.isep.presenthelper.R;
import com.presenthelper.isep.presenthelper.api.MainApi;
import com.presenthelper.isep.presenthelper.api.entities.Presentation;

import java.io.File;

public class ShowSlidesActivity extends AppCompatActivity {
    private static final String TAG = "MainMenuActivity";
    public static final String MODE_KEY = "mode_type";
    public static final String PRESENTATION_KEY = "presentation_type";
    public static final String QRCODE_KEY = "qrcode_key";
    public static final int MODE_LIVE_PRESENTER = 0;
    public static final int MODE_LIVE_SLIDES = 1;
    public static final int MODE_VIEW_SLIDES = 2;
    public static String presentationId;

     float x1=0,x2=0;
    final int MIN_DISTANCE = 150;

    private static  MainApi _mainApi;
    private static ImageView _slideImage;
    private Button _backwardBtn;
    private Button _forwardBtn;
    private Button _endBtn;
    private int _mode;
    private Presentation _presentation;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);




        _mainApi = MainApi.getInstance(this.getApplicationContext());

        Bundle bundle = getIntent().getExtras();
        _mode = bundle.getInt(MODE_KEY);
        if (_mode == MODE_LIVE_SLIDES) {
            presentationId = bundle.getString(QRCODE_KEY);
            presentationId = presentationId.replace("PresentHelper","");
        }else{
            presentationId = bundle.getString(PRESENTATION_KEY);
        }

        _mainApi.getPresentations(new Runnable() {
            @Override
            public void run() {
                _presentation = _mainApi.getPresentation(presentationId);
                _mainApi.startLivePresentation(_presentation.getId(), new Runnable(){
                    @Override
                    public void run() {
                        _mainApi.getCurrentSlide(presentationId, new Runnable() {
                            @Override
                            public void run() {
                                File imgFile = new  File("/storage/emulated/0/image.jpg");
                                if(!imgFile.canRead())
                                    imgFile = new File("/storage/sdcard0/image.jpg");
                                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                                _slideImage.setImageBitmap(myBitmap);
                            }
                        }, new Runnable() {
                            @Override
                            public void run() {

                            }
                        });

                    }},new Runnable(){
                    @Override
                    public void run() {

                    }});
            }
        }, new Runnable() {
            @Override
            public void run() {

            }
        });

        setContentView(R.layout.slide_view);

        _slideImage = (ImageView) findViewById(R.id.img_slide);
        _backwardBtn = (Button) findViewById(R.id.btn_backward);
        _forwardBtn = (Button) findViewById(R.id.btn_forward);
        _endBtn = (Button) findViewById(R.id.btn_end_slide);


        if(_mode != MODE_LIVE_SLIDES) {
            _slideImage.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {

                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            x1 = event.getX();
                            break;
                        case MotionEvent.ACTION_UP:
                            x2 = event.getX();
                            float deltaX = x2 - x1;

                            if (Math.abs(deltaX) > MIN_DISTANCE) {
                                // Left to Right swipe action
                                if (x2 > x1) {
                                    previousSlide();
                                }

                                // Right to left swipe action
                                else {
                                    nextSlide();
                                }

                            } else {
                                // consider as something else - a screen tap for example
                            }
                            break;
                    }
                    return true;
                }
            });
        }else{
            _mainApi.subscribeLivePresentation(presentationId, new Runnable() {
                @Override
                public void run() {

                }
            }, new Runnable() {
                @Override
                public void run() {

                }
            });
        }


        if(_mode == MODE_LIVE_PRESENTER || _mode == MODE_LIVE_SLIDES) {
            setLiveMode();
        }
        if(_mode == MODE_LIVE_PRESENTER || _mode == MODE_VIEW_SLIDES) {
            setOffileMode();
        }

        _endBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(_mode == MODE_LIVE_SLIDES) {
                    _mainApi.unsubscribeLivePresentation(presentationId, new Runnable() {
                        @Override
                        public void run() {
                            onSuccess();
                        }
                    }, new Runnable() {
                        @Override
                        public void run() {

                        }
                    });
                }else{
                    _mainApi.endPresentation(presentationId, new Runnable() {
                        @Override
                        public void run() {

                            onSuccess();
                        }
                    }, new Runnable() {
                        @Override
                        public void run() {

                        }
                    });
                }

            }
        });
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    private void setLiveMode() {
       // _backwardBtn.setVisibility(View.GONE);
       // _forwardBtn.setVisibility(View.GONE);
        _forwardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextSlide();
            }
        });
        watchPushNotifications();
    }

    public static void watchPushNotifications() {
        // [TBD] wait for push notification and update image
        System.out.print("Ola");
        getCurrentSlide();
    }

    private static void getCurrentSlide(){

                _mainApi.getCurrentSlide(presentationId, new Runnable() {
                    @Override
                    public void run() {
                        File imgFile = new  File("/storage/emulated/0/image.jpg");
                        if(!imgFile.canRead())
                            imgFile = new File("/storage/sdcard0/image.jpg");
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        _slideImage.setImageBitmap(myBitmap);
                    }
                }, new Runnable() {
                    @Override
                    public void run() {

                    }
                });
    }



    private void setOffileMode() {
        _backwardBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(_mode == MODE_LIVE_PRESENTER) {previousLiveSlide();}
                if(_mode == MODE_VIEW_SLIDES) {previousSlide();}
            }
        });
        _forwardBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(_mode == MODE_LIVE_PRESENTER) {nextLiveSlide();}
                if(_mode == MODE_VIEW_SLIDES) {previousSlide();}
            }
        });
    }

    private void previousLiveSlide() {

    }

    private void nextLiveSlide() {
        nextSlide();
    }

    private void previousSlide() {
        _mainApi.previusSlide(presentationId, new Runnable() {
            @Override
            public void run() {
                _mainApi.getCurrentSlide(presentationId, new Runnable() {
                    @Override
                    public void run() {
                        File imgFile = new  File("/storage/emulated/0/image.jpg");
                        if(!imgFile.canRead())
                            imgFile = new File("/storage/sdcard0/image.jpg");
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        _slideImage.setImageBitmap(myBitmap);
                    }
                }, new Runnable() {
                    @Override
                    public void run() {

                    }
                });
            }
        }, new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    private void nextSlide() {
        _mainApi.nextSlide(presentationId, new Runnable() {
            @Override
            public void run() {
                _mainApi.getCurrentSlide(presentationId, new Runnable() {
                    @Override
                    public void run() {
                        File imgFile = new  File("/storage/emulated/0/image.jpg");
                        if(!imgFile.canRead())
                            imgFile = new File("/storage/sdcard0/image.jpg");
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        _slideImage.setImageBitmap(myBitmap);
                    }
                }, new Runnable() {
                    @Override
                    public void run() {

                    }
                });
            }
        }, new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    private void onSuccess() {
        setResult(RESULT_OK, null);
        finish();
    }

    private void onFailed() {
        Toast.makeText(getBaseContext(), "Failed", Toast.LENGTH_LONG).show();
    }
}
