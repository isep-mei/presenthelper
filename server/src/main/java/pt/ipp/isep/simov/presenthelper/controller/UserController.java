package pt.ipp.isep.simov.presenthelper.controller;

import java.security.Principal;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pt.ipp.isep.simov.presenthelper.domain.Presentation;
import pt.ipp.isep.simov.presenthelper.domain.User;
import pt.ipp.isep.simov.presenthelper.repository.PresentationRepository;
import pt.ipp.isep.simov.presenthelper.repository.RoleRepository;
import pt.ipp.isep.simov.presenthelper.repository.UserRepository;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private PresentationRepository presentationRepository;
	
	@RequestMapping(value = "/presenter", method = RequestMethod.PUT)
	@PreAuthorize("isAuthenticated()")
	public User registeUserAsPresenter(Principal principal) {
		// TODO
		// Shuld implement the service insted of the repository
		User user = userRepository.findByUsername(principal.getName());
		user.getRoles().add(roleRepository.findByRoleName("PRESENTER_USER"));
		return userRepository.save(user);
	}

	@RequestMapping(value = "/viewer", method = RequestMethod.PUT)
	@PreAuthorize("isAuthenticated()")
	public User registeUserAsViewer(Principal principal) {
		// TODO
		// Shuld implement the service insted of the repository
		User user = userRepository.findByUsername(principal.getName());
		user.getRoles().add(roleRepository.findByRoleName("VIEWER_USER"));
		return userRepository.save(user);
	}
	
	@RequestMapping(value = "/presentations", method = RequestMethod.GET)
	@PreAuthorize("hasAuthority('PRESENTER_USER')")
	public List<Presentation> getPresentationsFromUsers(Principal principal) {
		User user = userRepository.findByUsername(principal.getName());
		return presentationRepository.findByOwner(user);
	}
	
	@RequestMapping(value = "/token", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public User updateToke(Principal principal, @RequestBody String token) {
		User user = userRepository.findByUsername(principal.getName());
		
		user.setToken(new JSONObject(token).getString("token"));
		return userRepository.save(user);
	}

}
