package com.presenthelper.isep.presenthelper.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.presenthelper.isep.presenthelper.R;
import com.presenthelper.isep.presenthelper.api.MainApi;

public class SignupActivity extends AppCompatActivity {
    private static final String TAG = "SignupActivity";

    private MainApi _mainApi;
    private EditText _firstNameText;
    private EditText _lastNameText;
    private EditText _usernameText;
    private EditText _passwordText;
    private EditText _reEnterPasswordText;
    private Button _signupButton;
    private TextView _loginLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);

        _mainApi = MainApi.getInstance(this.getApplicationContext());
        _firstNameText = (EditText) findViewById(R.id.input_first_name);
        _lastNameText = (EditText) findViewById(R.id.input_last_name);
        _usernameText = (EditText) findViewById(R.id.input_username);
        _passwordText = (EditText) findViewById(R.id.input_password);
        _reEnterPasswordText = (EditText) findViewById(R.id.input_reEnterPassword);
        _signupButton = (Button) findViewById(R.id.btn_signup);
        _loginLink = (TextView) findViewById(R.id.link_login);

        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    public void signup() {
        Log.d(TAG, "Signup");

        final String firstName = _firstNameText.getText().toString();
        final String lastName = _lastNameText.getText().toString();
        final String username = _usernameText.getText().toString();
        final String password = _passwordText.getText().toString();
        final String reEnterPassword = _reEnterPasswordText.getText().toString();

        if (!validate(firstName, lastName, username, password, reEnterPassword)) {
            onSignupFailed();
            return;
        }

        _signupButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();

        _mainApi.registerUser(firstName, lastName, username, password,
                new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        _mainApi.addUserAsPresenter(username, password, new Runnable() {
                            @Override
                            public void run() {
                                _mainApi.addUserAsViewr(username, password, new Runnable() {
                                    @Override
                                    public void run() {
                                        onSignupSuccess(username, password);
                                    }
                                }, new Runnable() {
                                    @Override
                                    public void run() {
                                        onSignupFailed();
                                    }
                                });
                            }
                        }, new Runnable() {
                            @Override
                            public void run() {
                                onSignupFailed();
                            }
                        });


                    }
                },
                new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        onSignupFailed();
                    }
                }
        );
    }

    public void onSignupSuccess(String username, String password) {
        Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
        intent.putExtra(LoginActivity.USERNAME_KEY, username);
        intent.putExtra(LoginActivity.PASSWORD_KEY, password);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        _signupButton.setEnabled(true);
    }

    public boolean validate(String firstName, String lastName, String username, String password,
                            String reEnterPassword) {
        boolean valid = true;

        if (firstName.isEmpty() || firstName.length() < 4) {
            _firstNameText.setError("At least 4 characters");
            valid = false;
        } else {
            _firstNameText.setError(null);
        }

        if (lastName.isEmpty() || lastName.length() < 4) {
            _lastNameText.setError("At least 4 characters");
            valid = false;
        } else {
            _lastNameText.setError(null);
        }

        if (username.isEmpty() || username.length() < 4) {
            _usernameText.setError("At least $ characters");
            valid = false;
        } else {
            _usernameText.setError(null);
        }

        // To validate emails
        // !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("Should be between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        if (!reEnterPassword.equals(password)) {
            _reEnterPasswordText.setError("Password do not match");
            valid = false;
        } else {
            _reEnterPasswordText.setError(null);
        }

        return valid;
    }
}