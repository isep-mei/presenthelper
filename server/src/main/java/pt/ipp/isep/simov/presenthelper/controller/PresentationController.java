package pt.ipp.isep.simov.presenthelper.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import net.glxn.qrgen.javase.QRCode;
import pt.ipp.isep.simov.presenthelper.domain.Presentation;
import pt.ipp.isep.simov.presenthelper.domain.User;
import pt.ipp.isep.simov.presenthelper.notification.SendNotification;
import pt.ipp.isep.simov.presenthelper.repository.PresentationRepository;
import pt.ipp.isep.simov.presenthelper.repository.UserRepository;

@RestController
@RequestMapping("/presentation")
public class PresentationController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PresentationRepository presentationRepository;

	@RequestMapping(method = RequestMethod.GET)
	@PreAuthorize("isAuthenticated()")
	public List<Presentation> getUserPresentations(Principal principal) {
        User user = userRepository.findByUsername(principal.getName());
        return presentationRepository.findByOwner(user);
	}

	@RequestMapping(method = RequestMethod.PUT)
	@PreAuthorize("hasAuthority('PRESENTER_USER')")
	public Presentation createPresentation(@RequestBody Presentation presentation, Principal principal) {
		User presentationUser = userRepository.findByUsername(principal.getName());
		presentation.setOwner(presentationUser);
		presentation.setPath("path !!!!");
		presentation.setSlides(25);
		presentationRepository.save(presentation);
		return presentation;
	}
	
	@RequestMapping(value = "/{id}/qrcode", method = RequestMethod.GET, produces = "image/png")
	public ResponseEntity<InputStreamResource> generateQRCode(@PathVariable("id") long id) throws IOException {
		
		//https://github.com/kenglxn/QRGen
		File file =  QRCode.from("PresentHelper"+id).file();
		 InputStream targetStream = new FileInputStream(file);
		return ResponseEntity
	            .ok()
	            .contentLength(file.length())
	            .contentType(
	                    MediaType.parseMediaType("application/octet-stream"))
	            .body(new InputStreamResource(targetStream));
	}
	
	@RequestMapping(value = "/{id}/live", method = RequestMethod.PUT)
	@PreAuthorize("hasAuthority('PRESENTER_USER')")
	public Presentation startLivePresentation(Principal principal, @PathVariable("id") long id) {
		//TODO check is owner 
		Presentation presentation = presentationRepository.findOne(id);
		presentation.setLive(true);
		presentation.setCurrentSlide(0);
		presentationRepository.save(presentation);
		return presentation;
	}
	
	@RequestMapping(value = "/{id}/subscribe", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public Presentation subscribePresentation(Principal principal, @PathVariable("id") long id) {
		//TODO check is owner 
		Presentation presentation = presentationRepository.findOne(id);
		List<User> subscritors = presentation.getSubscriptions();
		subscritors.add(userRepository.findByUsername(principal.getName()));
		presentation.setSubscriptions(subscritors);
		presentationRepository.save(presentation);
		return presentation;
	}
	
	@RequestMapping(value = "/{id}/live", method = RequestMethod.DELETE)
	@PreAuthorize("hasAuthority('PRESENTER_USER')")
	public Presentation stopLivePresentation(Principal principal, @PathVariable("id") long id) {
		//TODO check is owner 
		Presentation presentation = presentationRepository.findOne(id);
		presentation.setLive(false);
		presentationRepository.save(presentation);
		return presentation;
	}
	
	@RequestMapping(value = "/{id}/live/next", method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('PRESENTER_USER')")
	public Presentation presentationLiveNext(Principal principal, @PathVariable("id") long id) {
		//TODO check if presentation isn't in the end
		Presentation presentation = presentationRepository.findOne(id);
		presentation.setCurrentSlide(presentation.getCurrentSlide()+1);
		presentationRepository.save(presentation);
		for(User user:presentation.getSubscriptions()) {
			SendNotification.sendNotification(user.getToken());
		}
		return presentation;
	}
	@RequestMapping(value = "/{id}/live/previus", method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('PRESENTER_USER')")
	public Presentation presentationLivePrevius(Principal principal, @PathVariable("id") long id) {
		//TODO check if presentation isn't in the begining
		Presentation presentation = presentationRepository.findOne(id);
		presentation.setCurrentSlide(presentation.getCurrentSlide()-1);
		presentationRepository.save(presentation);
		for(User user:presentation.getSubscriptions()) {
			SendNotification.sendNotification(user.getToken());
		}
		return presentation;
	}
	
	@RequestMapping(value = "/{id}/live", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> presentationLive(Principal principal, @PathVariable("id") long id) throws IOException {
		//TODO check is presentation is live
		Presentation presentation = presentationRepository.findOne(id);
		String path = presentation.getPath()+presentation.getCurrentSlide()+".jpg";
		File file = new File(path);
		InputStream targetStream = new FileInputStream(file);
		return ResponseEntity
	            .ok()
	            .contentLength(file.length())
	            .contentType(
	                    MediaType.parseMediaType("application/octet-stream"))
	            .body(new InputStreamResource(targetStream));
	}

	@RequestMapping(value = "/{id}/liveview", method = RequestMethod.GET)
	public String liveview(Principal principal, @PathVariable("id") long id, Model model) {
		//TODO check is presentation is live
		Presentation presentation = presentationRepository.findOne(id);
        model.addAttribute("presentation_id", presentation.getId());
        model.addAttribute("presentation_name", presentation.getName());
		return "liveview";
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('PRESENTER_USER')")
	public ResponseEntity<Presentation> createPresentation(Principal principal, @PathVariable("id") long id,
			@RequestParam("file") MultipartFile file) {
		Presentation presentation = presentationRepository.findOne(id);
		if(presentation != null) {
			if(presentation.getOwner().getUsername().equals(principal.getName())) {
				try {
					File dir = new File("presentations/"+id);
					dir.mkdir();
					PDDocument document = PDDocument.load(convert(file));
					PDFRenderer renderer = new PDFRenderer(document);
					for(int i = 0; i<document.getNumberOfPages(); i++) {
						BufferedImage image = renderer.renderImage(i);
						ImageIO.write(image, "JPEG", new File("presentations/"+id+"/"+i+".jpg"));
					}
					presentation.setSlides(document.getNumberOfPages());
					presentation.setPath("presentations/"+id+"/");
					presentationRepository.save(presentation);
					document.close();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else {
				return new ResponseEntity<Presentation>(HttpStatus.NOT_ACCEPTABLE);
			}
		}else {
			return new ResponseEntity<Presentation>(HttpStatus.NOT_ACCEPTABLE);
		}
		return new ResponseEntity(presentation, HttpStatus.OK);
	}

	public File convert(MultipartFile file) {
		try {
			File convFile = new File(file.getOriginalFilename());

			convFile.createNewFile();

			FileOutputStream fos = new FileOutputStream(convFile);
			fos.write(file.getBytes());
			fos.close();
			return convFile;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
