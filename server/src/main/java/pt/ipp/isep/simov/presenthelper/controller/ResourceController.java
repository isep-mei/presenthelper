package pt.ipp.isep.simov.presenthelper.controller;

import pt.ipp.isep.simov.presenthelper.domain.Presentation;
import pt.ipp.isep.simov.presenthelper.domain.RandomCity;
import pt.ipp.isep.simov.presenthelper.domain.Role;
import pt.ipp.isep.simov.presenthelper.domain.User;
import pt.ipp.isep.simov.presenthelper.repository.PresentationRepository;
import pt.ipp.isep.simov.presenthelper.repository.RoleRepository;
import pt.ipp.isep.simov.presenthelper.repository.UserRepository;
import pt.ipp.isep.simov.presenthelper.service.GenericService;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.imageio.ImageIO;

/**
 * Created by nydiarra on 06/05/17.
 */
@RestController
@RequestMapping("/")
public class ResourceController {

	

	@Autowired
	private GenericService userService;
	


	@RequestMapping(value = "/cities")
	@PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
	public List<RandomCity> getUser() {
		Collection<? extends GrantedAuthority> a = SecurityContextHolder.getContext().getAuthentication()
				.getAuthorities();
		return userService.findAllRandomCities();
	}

	

	

	

}
