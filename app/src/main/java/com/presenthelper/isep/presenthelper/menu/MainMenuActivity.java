package com.presenthelper.isep.presenthelper.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.presenthelper.isep.presenthelper.R;
import com.presenthelper.isep.presenthelper.api.MainApi;
import com.presenthelper.isep.presenthelper.api.entities.Presentation;

import java.util.ArrayList;
import java.util.List;

public class MainMenuActivity extends AppCompatActivity {
    private static final String TAG = "MainMenuActivity";
    private static final int PRESENTATION_ACTIVITY = 0;
    private static final int QRREAD_ACTIVITY = 1;
    private static final int SLIDE_ACTIVITY = 2;

    private MainApi _mainApi;
    private ListView _myPresentationsList;
    private ArrayAdapter<Presentation> presentationsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _mainApi = MainApi.getInstance(this.getApplicationContext());

        setContentView(R.layout.main_menu);

        _myPresentationsList = (ListView) findViewById(R.id.lst_presentations);
        setPresentationsList(_myPresentationsList);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void setPresentationsList(ListView presentationsListView) {
        List<Presentation> presentationsList = new ArrayList<>();
        presentationsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, presentationsList);

        registerForContextMenu(presentationsListView);
        presentationsListView.setAdapter(presentationsAdapter);
        _mainApi.getPresentations(
                new Runnable() {
                    @Override
                    public void run() {
                        updatePresentationsArray();
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {
                    }
                }
        );
    }

    private void updatePresentationsArray() {
        presentationsAdapter.clear();
        presentationsAdapter.addAll(_mainApi.getPresentations().values());
        presentationsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case QRREAD_ACTIVITY:
                onQRCodeResult(resultCode, data);
                break;
        }
        presentationsAdapter.notifyDataSetChanged();
    }

    private void onQRCodeResult(int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            String qrCode = data.getStringExtra(QRCode.QRCODE_KEY);
            Intent liveSlideShowIntent = new Intent(this, ShowSlidesActivity.class);
            liveSlideShowIntent.putExtra(ShowSlidesActivity.MODE_KEY, ShowSlidesActivity.MODE_LIVE_SLIDES);
            liveSlideShowIntent.putExtra(ShowSlidesActivity.QRCODE_KEY, qrCode);
            startActivityForResult(liveSlideShowIntent, SLIDE_ACTIVITY);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        _mainApi.getPresentations(
                new Runnable() {
                    @Override
                    public void run() {
                        updatePresentationsArray();
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {

                    }
                }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_create_new_presentation:
                // Toast.makeText(getApplicationContext(), "Create New Presentation", Toast.LENGTH_LONG).show();
                Intent newPresentationIntent = new Intent(this, PresentationActivity.class);
                newPresentationIntent.putExtra(PresentationActivity.MODE_KEY, PresentationActivity.MODE_ADD);
                startActivityForResult(newPresentationIntent, PRESENTATION_ACTIVITY);
                break;
            case R.id.action_assist_live:
                // Toast.makeText(getApplicationContext(), "Start Watching Live", Toast.LENGTH_LONG).show();
                Intent liveIntent = new Intent(this, QRCode.class);
                startActivityForResult(liveIntent, QRREAD_ACTIVITY);
                break;
            case R.id.action_settings:
                Toast.makeText(getApplicationContext(), "Settings Selected", Toast.LENGTH_LONG).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_presentation_context, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_start_live:
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                Toast.makeText(getApplicationContext(), "Starting Live with Presentation", Toast.LENGTH_LONG).show();
                Intent startLiveIntent = new Intent(this, ShowSlidesActivity.class);
                startLiveIntent.putExtra(ShowSlidesActivity.MODE_KEY, ShowSlidesActivity.MODE_LIVE_PRESENTER);

                startLiveIntent.putExtra(ShowSlidesActivity.PRESENTATION_KEY, presentationsAdapter.getItem(info.position).getId());
                startActivityForResult(startLiveIntent, SLIDE_ACTIVITY);
                break;
            case R.id.action_edit:
                // Toast.makeText(getApplicationContext(), "Edit Presentation", Toast.LENGTH_LONG).show();
                Intent editPresentationIntent = new Intent(this, PresentationActivity.class);
                editPresentationIntent.putExtra(PresentationActivity.MODE_KEY, PresentationActivity.MODE_EDIT);
                editPresentationIntent.putExtra(PresentationActivity.PRESENTATION_KEY, item.getTitle().toString());
                startActivityForResult(editPresentationIntent, PRESENTATION_ACTIVITY);
                break;
            case R.id.action_delete:
                Toast.makeText(getApplicationContext(), "Delete Presentation", Toast.LENGTH_LONG).show();
                // [TBD] Delete presentation from api and from list.
                break;
            case R.id.action_view_notes:
                Toast.makeText(getApplicationContext(), "View Presentation Notes", Toast.LENGTH_LONG).show();
                break;
            case R.id.action_view_slides:
                Toast.makeText(getApplicationContext(), "View Presentation Slides", Toast.LENGTH_LONG).show();
                break;
        }
        return super.onContextItemSelected(item);
    }
}
