CREATE TABLE random_city (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE app_role (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  description varchar(255) DEFAULT NULL,
  role_name varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
);


CREATE TABLE app_user (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  first_name varchar(255) NOT NULL,
  last_name varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  username varchar(255) NOT NULL,
  token varchar(255),
  PRIMARY KEY (id)
);


CREATE TABLE user_role (
  user_id bigint(20) NOT NULL,
  role_id bigint(20) NOT NULL,
  CONSTRAINT FK859n2jvi8ivhui0rl0esws6o FOREIGN KEY (user_id) REFERENCES app_user (id),
  CONSTRAINT FKa68196081fvovjhkek5m97n3y FOREIGN KEY (role_id) REFERENCES app_role (id)
);

CREATE TABLE app_presentation (
	id bigint(20) NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	path varchar(255) NOT NULL,
	slides int NOT NULL,
	OWNER_ID bigint(20) NOT NULL,
	live boolean NULL,
	currentSlide int NOT NULL,
  	PRIMARY KEY (id)
);

CREATE TABLE presentation_user (
  presentation_id bigint(20) NOT NULL,
  user_id bigint(20) NOT NULL,
  CONSTRAINT FK859n2jvi8iaaaaarl0esws6o FOREIGN KEY (presentation_id) REFERENCES app_presentation (id),
  CONSTRAINT FK859n2jvi8ibbbbbrl0esws6o FOREIGN KEY (user_id) REFERENCES app_user (id)
);



