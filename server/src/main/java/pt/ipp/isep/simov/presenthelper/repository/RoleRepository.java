package pt.ipp.isep.simov.presenthelper.repository;

import pt.ipp.isep.simov.presenthelper.domain.Role;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by nydiarra on 06/05/17.
 */
public interface RoleRepository extends CrudRepository<Role, Long> {
	Role findByRoleName(String roleName);
}
