package com.presenthelper.isep.presenthelper.api.entities;

public class Presentation {
    private String _title;
    private String _id;

    public Presentation() {
    }

    public Presentation(String title, String id) {
        this();
        setTitle(title);
        setId(id);
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        this._title = title;
    }

    public String getId() {return _id;}

    public void setId(String id){this._id = id;}

    @Override
    public String toString() {
        return this._title;
    }
}
