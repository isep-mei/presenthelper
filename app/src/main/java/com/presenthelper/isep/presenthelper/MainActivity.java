package com.presenthelper.isep.presenthelper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.presenthelper.isep.presenthelper.api.MainApi;
import com.presenthelper.isep.presenthelper.login.LoginActivity;
import com.presenthelper.isep.presenthelper.menu.MainMenuActivity;

public class MainActivity extends AppCompatActivity {
    private final int LOGIN_ACTIVITY = 0;
    private final int MAIN_MENU_ACTIVITY = 1;

    private MainApi _mainApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _mainApi = MainApi.getInstance(this.getApplicationContext());

        Intent intent = new Intent(this, LoginActivity.class);
        startActivityForResult(intent, LOGIN_ACTIVITY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (LOGIN_ACTIVITY): loginHandler(resultCode, data); break;
            case (MAIN_MENU_ACTIVITY): mainMenuHandler(resultCode, data); break;
        }
    }

    private void loginHandler(int resultCode, Intent data) {
        switch (resultCode) {
            case Activity.RESULT_OK:
                Intent intent = new Intent(this, MainMenuActivity.class);
                startActivityForResult(intent, MAIN_MENU_ACTIVITY);
                break;
            case Activity.RESULT_CANCELED:
                finish();
                break;
        }
    }

    private void mainMenuHandler(int resultCode, Intent data) {
        finish();
    }
}